# спсики впорядковані

a = [1, 2, 3]
b = [3, 1, 2]

print(a == b)

print(a.__eq__(b))

# Списки можуть містити довільні об'єкти

c = [1, 2.4, '', None, ['ddd', 456]]

d = [str, len, bool]

e = [a, b, c]

for i in d:
    for elem in e:
        print(i(elem))

            # bool(obj) --> obj.__bool__()
bool[None] == True

# доступ - за індксом

a[2]

# списки - змінні


def fff(input_list=None):
    if input_list == None:
        input_list = []
    input_list += ["###"]
    return input_list

# заміна кількох значень списку

# a[m:n] = <iterable>